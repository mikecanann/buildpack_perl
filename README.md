

Predix buildpack for Perl applications
======================

This is a Predix buildpack that runs any PSGI based web applications using Starman.

Usage
-----

Example usage for [OTRS](https://github.com/OTRS/otrs):


    $ git clone https://github.com/OTRS/otrs.git
    $ cd otrs

    find the tag for the version of OTRS you want to run `git tag -l`, using 3.3.10
    $ git checkout rel-3_3_10

In this folder, create a file named `cpanfile` with the following contents:

```perl
requires 'Archive::Tar';
requires 'Crypt::Eksblowfish::Bcrypt';
requires 'Crypt::SSLeay';
requires 'Date::Format';
requires 'DBI';
requires 'DBD::mysql';
requires 'Encode::HanExtra';
requires 'IO::Socket::SSL';
requires 'JSON::XS';
requires 'List::Util::XS';
requires 'LWP::UserAgent';
requires 'Mail::IMAPClient';
requires 'IO::Socket::SSL';
requires 'Net::DNS';
requires 'Net::LDAP';
requires 'Net::SSL';
requires 'PDF::API2';
requires 'Compress::Zlib';
requires 'Text::CSV_XS';
requires 'Time::HiRes';
requires 'XML::Parser';
requires 'YAML::XS';
requires 'DateTime';
requires 'Template';
requires 'DBD::Pg';
```

Create a Postgres service instance on the Predix.io web page, and bind to it with the following command

    $ cf bind-service [app-name] [database-name]
    $ cf restage [app-name]

    Access url:
      http://app_name.predix.io/index.pl


Create the config files:
````
    $ cp Kernel/Config.pm.dist Kernel/Config.pm

    #This file is only in the older version:
    $ cp Kernel/Config/GenericAgent.pm.dist Kernel/Config/GerericAgent.pm
````

Change the application home Directory

    $ vim Kernel/Config.pm

    $Self->{Home} = '/home/vcap/app';


Edit the Database settings in `Kernel\Config.pm`

````perl
    use JSON;
````

...


````perl 
    # ---------------------------------------------------- #
    # database settings                                    #
    # ---------------------------------------------------- #


    my $vcap_services = decode_json($ENV{'VCAP_SERVICES'});


    # The database host
    $Self->{DatabaseHost} = $vcap_services->{'postgres'}[0]{'credentials'}{'host'};

    # The database name
    $Self->{Database} = $vcap_services->{'postgres'}[0]{'credentials'}{'database'};

    # The database user
    $Self->{DatabaseUser} = $vcap_services->{'postgres'}[0]{'credentials'}{'username'};

    # The password of database user. You also can use bin/otrs.CryptPassword.pl
    # for crypted passwords
    $Self->{DatabasePw} = $vcap_services->{'postgres'}[0]{'credentials'}{'password'};

    # The database DSN for MySQL ==> more: "perldoc DBD::mysql"
    #$Self->{DatabaseDSN} = "DBI:mysql:database=$Self->{Database};host=$Self->{DatabaseHost};";

    # The database DSN for PostgreSQL ==> more: "perldoc DBD::Pg"
    # if you want to use a local socket connection
    #$Self->{DatabaseDSN} = "DBI:Pg:dbname=$Self->{Database};";
    # if you want to use a TCP/IP connection
    $Self->{DatabaseDSN} = "DBI:Pg:dbname=$Self->{Database};host=$Self->{DatabaseHost};";

````




Push the application from your local directory to cloud foundry

    $ cf push [app-name] -b https://github.com/mikecanann/buildpack_perl.git



Use the command ````cf env [app-name]``` to see the database credentials for the installer.pl step below.



Create the postgres default database schema by going to the following URL:
````
      http://[app_name].predix.io/installer.pl
````

Select "PostgreSQL" and "Use an existing database for OTRS"

Then enter the Database credentials from the `cf env` command above.

After setting up the database edit `Kernel\Config.pm`

````perl
    # turn off the web installer
    $Self->{SecureMode} = 1;
````


Login to the application with the default credentials:
````
user: root@localhost
pw: root
````
Change the root password first and then create a new agent for the rest of the configuration.
